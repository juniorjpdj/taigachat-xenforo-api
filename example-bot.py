﻿# coding: utf-8 
import datetime, sys, shoutApi, time
from threading import Thread

chathost = "http://bukkit.pl/shoutbox"
login = "bot-login"
password = "bot-password"
ops = ['predefined','op','names']
quit = False

if sys.platform == "win32":
  encoding = sys.stdout.encoding
else:
  encoding = "utf-8"

class asyncRun(Thread):
  def __init__(self, what, *args):
    Thread.__init__(self)
    self.what = what
    self.args = args
    self.start()
  def run(self):
    self.what(*self.args)

def printP(msg):
  print(unicode(msg).encode(encoding))

def send(msg):
  User.send(unicode(msg).encode('utf-8'))

def onJoin(nick):
  printP(u'{t} {n} dołączył do czatu'.format(t=datetime.datetime.now().strftime('[%H:%M:%S]'), n=nick))

def onLeave(nick):
  printP(u'{t} {n} opuścił czat'.format(t=datetime.datetime.now().strftime('[%H:%M:%S]'), n=nick))

def onMsg(time, sender, msg, new):
  printP(u'{t} {n}: {m}'.format(t=datetime.datetime.fromtimestamp(int(time)).strftime('[%H:%M:%S]'), n=sender, m=msg))
  if msg.find('!') == 0 and new:
    args = msg.split(' ')
    asyncRun(onCommand, sender, args[0][1:], args[1:])

def onCommand(sender, command, args):
  if command.lower() == 'help':
    send(u"Lista dostępnych komend:\n"
         u"!help - wyświetla ten komunikat\n"
         u"!ping - odsyła wiadomość pong\n"
         u"!botquit - wyłącza bota\n"
         u"!ops - wyswietla liste operatorow\n"
         u"!op NICK - dodaje NICK do listy operatorow\n"
         u"!list - wyswietla liste graczy")
  elif command.lower() == 'ping':
    send(u'PONG! - specialnie dla: ' + sender)
  elif command.lower() == 'ops':
    oplist = u''
    for op in ops:
      oplist += ' ' + op
    send(u'Bot ma nastepujacych operatorów:'+oplist)
  elif command.lower() == 'op':
    if len(args) == 1:
      if sender in ops:
        ops.append(args[0])
        send(args[0] + u' został operatorem bota!')
      else:
        send(sender + u' - nie jestes operatorem bota')
    else:
      send(sender + u' - błędna ilość parametrów')
  elif command.lower() == 'botquit':
    if sender in ops:
      send(u'Wylaczanie bota')
      botQuit()
    else:
      send(sender + u' - nie jestes operatorem bota')
  elif command.lower() == 'list':
    usrlist = u''
    for usr in List.list():
      usrlist += u' ' + usr
    send(u'Lista graczy uczestniczących w czacie:'+usrlist)
  else:
    send(sender + u' - nie ma takiej komendy ;c')

def botQuit():
  printP(datetime.datetime.now().strftime('[%H:%M:%S] ')+u'[INFO] Wyłączanie bota')
  try:
    send(u'[INFO] Wylaczanie bota')
  except:
    pass
  List.stop()
  Reciever.stop()
  User.logout()
  global quit
  quit = True

User = shoutApi.ChatUser(chathost)
User.login(login, password)
Reciever = shoutApi.ChatReciever(chathost)
List = shoutApi.ChatUserList(chathost)
Reciever.regHandler('msg', onMsg)
List.regHandler('leave', onLeave)
List.regHandler('join', onJoin)

send(u'[INFO] Bot zalogowany, oczekiwanie na komendy - lista pod komendą !help')

while 1:
  try:
    time.sleep(1)
    if quit:
      break
  except:
    botQuit()
sys.exit()