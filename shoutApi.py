from threading import Thread, Event
from HTMLParser import HTMLParser
from datetime import datetime
from time import sleep
from urlparse import urljoin
from collections import OrderedDict
from copy import deepcopy
import sys
import urllib
import urllib2
import json
import re
import cookielib
import traceback


class SendMessageException(Exception):
    def __init__(self, msg):
        Exception.__init__(self)
        self.msg = msg

    def __str__(self):
        return self.msg


class UnknownHandlerException(Exception):
    def __init__(self):
        Exception.__init__(self)


class LogInException(Exception):
    def __init__(self):
        Exception.__init__(self)


class User(object):
    def __init__(self, id, nickname, avatarurl=None):
        self.id = int(id)
        self.nickname = nickname
        self.avatarurl = avatarurl

    def __str__(self):
        return u"{} ({})".format(self.nickname, self.id)

    def __repr__(self):
        return "User(id={}, nickname={})".format(self.id, self.nickname)

    def __eq__(self, other):
        return type(self) == type(other) and self.__dict__ == other.__dict__


class Message(object):
    def __init__(self, id, time, author, text):
        self.id = id
        self.author = author
        self.time = time
        self.text = text

    def __str__(self):
        return u"{} {}: {} ({})".format(self.time, self.author, self.text, self.id)

    def __repr__(self):
        return "Message(time={}, id={}, author={})".format(self.time, self.id, self.author)

    def __eq__(self, other):
        return type(self) == type(other) and self.__dict__ == other.__dict__


class ChatReciever(Thread):
    def __init__(self, url, timeout=10):
        Thread.__init__(self)
        self.onMsg = []
        self.onEdit = []
        self.onDelete = []
        self.onJoin = []
        self.onLeave = []
        self._chatHistory = OrderedDict()
        self.users = []
        self.url = url
        self.timeout = timeout
        self._stop = Event()

    def regHandler(self, handlertype, func):
        if handlertype == 'msg':
            self.onMsg.append(func)
        elif handlertype == 'edit':
            self.onEdit.append(func)
        elif handlertype == 'delete':
            self.onDelete.append(func)
        elif handlertype == 'join':
            self.onJoin.append(func)
        elif handlertype == 'leave':
            self.onLeave.append(func)
        else:
            raise UnknownHandlerException()

    def run(self):
        lastref = 0
        data = urllib2.urlopen(self.url).read()
        room = data.split('taigachat.room = parseInt("')[1].split('"')[0] or '1'
        maxmsg = int(data.split('taigachat.limit = parseInt("')[1].split('"')[0] or '50')
        fake = 0 if data.split('taigachat.fake = ')[1].split(';')[0] == 'false' else 1
        sidebar = 0 if data.split('taigachat.sidebar = ')[1].split(';')[0] == 'false' else 1
        reftime = int(data.split('taigachat.focusedRefreshTime = parseInt("')[1].split('"')[0] or '3')
        geturl = data.split('taigachat.url = "')[1].split('"')[0]
        geturl = urljoin(self.url, geturl if geturl.startswith('/') else '/' + geturl)
        while 1:
            if self._stop.isSet():
                break
            try:
                output = urllib2.urlopen(urllib2.Request(geturl, urllib.urlencode(
                    {"lastrefresh": lastref, "sidebar": sidebar, "fake": fake, "room": room})),
                                         timeout=self.timeout).read()
                new = bool(lastref)
                outputjson = json.loads(output.replace('\n', ''))
                lastref = outputjson["lastrefresh"]

                users = map(lambda x: User(x[0], re.sub('<[^>]*>', '', HTMLParser().unescape(x[1]))),
                            re.findall('<a.*?\.(.*?)\/\".*?username\">(.*?)<\/a>',
                                       outputjson['onlineUsers'].replace('\n', '')))
                if users != self.users and users != [] and self.users != []:
                    self.userslock = True
                    for a in users:
                        try:
                            self.users.remove(a)
                        except ValueError:
                            for handler in self.onJoin:
                                handler(a)
                    for a in self.users:
                        for handler in self.onLeave:
                            handler(a)
                if users != []:
                    self.users = users
                    self.userslock = False

                for msg in outputjson['messages']:
                    parsedmsg = list(re.findall(
                        'data-userid="(.*?)".*?src="(.*?)".*?alt="(.*?)".*?data-time="(.*?)".*?messagetext ugc\'>([\s\S]*?)<\/div>',
                        msg['html'])[0])
                    parsedmsg[2] = HTMLParser().unescape(parsedmsg[2])
                    parsedmsg[4] = re.sub('<img.*?src="(.*?)".*?alt="(.*?)".*?>', (
                        lambda x: '[IMG] {x} [/IMG]'.format(x=x.group(1)) if x.group(2) == '[&#x200B;IMG]' else x.group(
                            2)),
                                          parsedmsg[4])
                    parsedmsg[4] = re.sub('<a.*?href="(.*?)".*?>.*?</a>', r'\1', parsedmsg[4])
                    parsedmsg[4] = re.sub('<br \/>', '\n', parsedmsg[4])
                    parsedmsg[4] = re.sub('<[^>]*>', '', parsedmsg[4])
                    parsedmsg[4] = HTMLParser().unescape(parsedmsg[4])
                    msg = Message(msg['id'], datetime.fromtimestamp(int(parsedmsg[3])),
                                  User(parsedmsg[0], parsedmsg[2], parsedmsg[1]), parsedmsg[4])

                    if msg.id in self._chatHistory:
                        if not self._chatHistory[msg.id] == msg:
                            oldmsg = self._chatHistory[msg.id]
                            self._chatHistory[msg.id] = msg
                            for handler in self.onEdit:
                                handler(oldmsg, msg)
                    else:
                        self._chatHistory[msg.id] = msg
                        while len(self._chatHistory) > maxmsg:
                            self._chatHistory.popitem(False)
                        for handler in self.onMsg:
                            handler(msg, new)

                for msg in set(self._chatHistory.keys()).difference(outputjson['messageIds']):
                    for handler in self.onDelete:
                        handler(self._chatHistory[msg])
                    del (self._chatHistory[msg])
            except Exception as e:
                print traceback.format_exc()
                sys.stderr.write('Error while getting messages: {e}\n'.format(e=str(e)))
            if self._stop.isSet():
                break
            sleep(reftime)

    @property
    def userList(self):
        while self.userslock or self.users == []:
            sleep(0.1)
        return deepcopy(self.users)

    @property
    def chatHistory(self):
        return deepcopy(self._chatHistory)

    def stop(self):
        self._stop.set()


class ChatSender(object):
    def __init__(self, url, timeout=10):
        object.__init__(self)
        self.url = url
        self.timeout = timeout
        self.token = False
        self.sidebar = None
        self.room = None
        self.posturl = None
        self.cj = cookielib.LWPCookieJar()
        self.urlopener = urllib2.build_opener(urllib2.HTTPCookieProcessor(self.cj))

    def login(self, login, password):
        if not self.isLoggedIn():
            data = urllib2.urlopen(self.url).read()
            self.room = data.split('taigachat.room = parseInt("')[1].split('"')[0] or '1'
            self.sidebar = 0 if data.split('taigachat.sidebar = ')[1].split(';')[0] == 'false' else 1
            self.posturl = data.split('taigachat.url_post = "')[1].split('"')[0]
            self.posturl = urljoin(self.url, self.posturl if self.posturl.startswith('/') else '/' + self.posturl)
            loginurl = urljoin(self.url, '/login/login')
            token = self.urlopener.open(urllib2.Request(loginurl, urllib.urlencode(
                {"login": login, "password": password, "cookie_check": "0", "register": "0", "remember": "1"})),
                                        timeout=self.timeout)
            if token.url == loginurl:
                raise LogInException('Wrong username or password')
            token = token.read()
            token = token[token.find('name="_xfToken') + 23:]
            self.token = token[:token.find('"')]

    def isLoggedIn(self):
        if self.token:
            page = self.urlopener.open(urllib2.Request(urljoin(self.url, '/login/'), urllib.urlencode(
                {"_xfResponseType": "json", "_xfToken": self.token})), timeout=self.timeout).read()
            if page.find('_redirect') > -1:
                return True
            else:
                self.token = False
                return False
        else:
            return False

    def logout(self):
        if self.token:
            self.urlopener.open(
                urljoin(self.url, '/logout/?' + urllib.urlencode({"_xfResponseType": "json", "_xfToken": self.token})))
            self.token = False

    def send(self, msg, retry=3):
        if self.token:
            page = self.urlopener.open(urllib2.Request(self.posturl,
                                                       urllib.urlencode(
                                                           {"message": msg, "sidebar": self.sidebar,
                                                            "room": self.room, "color": "",
                                                            "_xfToken": self.token,
                                                            "_xfResponseType": "xml"})),
                                       timeout=self.timeout).read()
            if page.find('unrepresentable') == -1:
                raise SendMessageException('Unable to send message: "{m}"'.format(m=msg))
        else:
            raise SendMessageException('Not logged in')